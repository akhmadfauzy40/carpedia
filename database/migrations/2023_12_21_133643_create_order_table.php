<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('customer');
            $table->string('nomor_telpon');
            $table->text('alamat');
            $table->integer('diskon');
            $table->unsignedBigInteger('total');
            $table->unsignedBigInteger('sales_id');
            $table->unsignedBigInteger('cars_id');
            $table->timestamps();

            $table->foreign('sales_id')->references('id')->on('users');
            $table->foreign('cars_id')->references('id')->on('cars');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order');
    }
};
