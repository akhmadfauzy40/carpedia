<!DOCTYPE html>
<html>

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>About Us</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/bootstrap.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/style.css') }}">
    <!-- Responsive-->
    <link rel="stylesheet" href="{{ asset('plugins/css/responsive.css') }}">
    <!-- fevicon -->
    <link rel="icon" href="{{ asset('images/carpedia_logo2_justlogo.png') }}" type="image/gif" />
    <!-- font css -->
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&family=Raleway:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/css/jquery.mCustomScrollbar.min.css') }}">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
</head>

<body>
    <!-- banner section start -->
    <div class="about_section layout_padding">
        <div class="container">
            <div class="about_section_2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image_iman"><img src="{{ asset('storage/' . $data->image) }}" class="about_img">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_taital_box">
                            <h1 class="about_taital">{{ $data->merk }} <span
                                    style="color: #fe5b29;">{{ $data->model }}</span></h1>
                            <p class="about_text">{{ $data->deskripsi }}</p>
                            <ul>
                                <li>
                                    <p class="about_text">Keluaran Tahun : {{ $data->tahun }}</p>
                                </li>
                                <li>
                                    <p class="about_text">Jenis Mobil : {{ $data->jenis }}</p>
                                </li>
                                <li>
                                    <p class="about_text">Jenis Transmisi : {{ $data->transmisi }}</p>
                                </li>
                                <li>
                                    <p class="about_text">Warna Mobil : {{ $data->warna }}</p>
                                </li>
                                <li>
                                    <p class="about_text">Harga Mobil : Rp.
                                        {{ number_format($data->harga, 0, ',', '.') }}</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="read_bt"><a href="https://wa.me/{{ Illuminate\Support\Str::startsWith($data->contact_sales, '0') ? '+62' . substr($data->contact_sales, 1) : $data->contact_sales }}" target="_blank">Hubungi Penjual</a></div>
    </div>
    <!-- contact section end -->
    <!-- footer section start -->
    <div class="footer_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footeer_logo"><img src="{{ asset('images/carpedia_logo2.png') }}"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer section end -->
    <!-- copyright section start -->
    <div class="copyright_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="copyright_text">Carpedia Team</p>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright section end -->
    <!-- Javascript files-->
    <script src="{{ asset('plugins/js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/js/popper.min.js') }}"></script>
    <script src="{{ asset('plugins/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ asset('plugins/js/plugin.js') }}"></script>
    <!-- sidebar -->
    <script src="{{ asset('plugins/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('plugins/js/custom.js') }}"></script>
</body>

</html>
