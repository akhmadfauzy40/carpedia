<!DOCTYPE html>
<html>

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>About Us</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/bootstrap.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/style.css') }}">
    <!-- Responsive-->
    <link rel="stylesheet" href="{{ asset('plugins/css/responsive.css') }}">
    <!-- fevicon -->
    <link rel="icon" href="{{ asset('images/carpedia_logo2_justlogo.png') }}" type="image/gif" />
    <!-- font css -->
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&family=Raleway:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/css/jquery.mCustomScrollbar.min.css') }}">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
</head>

<body>
    <!-- header section start -->
    <div class="header_section">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"href="/"><img src="{{ asset('images/carpedia_logo2.png') }}"
                        style="max-height: 50px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/#showroom">Showroom</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- header section end -->
    <div class="call_text_main">
        <div class="container">
            <div class="call_taital">
                <div class="call_text"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><span
                            class="padding_left_15">Location</span></a></div>
                <div class="call_text"><a href="#"><i class="fa fa-phone" aria-hidden="true"></i><span
                            class="padding_left_15">(+71) 8522369417</span></a></div>
                <div class="call_text"><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i><span
                            class="padding_left_15">demo@gmail.com</span></a></div>
            </div>
        </div>
    </div>
    <!-- banner section start -->
    <div class="about_section layout_padding">
        <div class="container">
           <div class="about_section_2">
              <div class="row">
                 <div class="col-md-6">
                    <div class="image_iman"><img src="{{ asset('plugins/images/about-img.png') }}" class="about_img"></div>
                 </div>
                 <div class="col-md-6">
                    <div class="about_taital_box">
                       <h1 class="about_taital">About <span style="color: #fe5b29;">Us</span></h1>
                       <p class="about_text">Gun Mobilindo, sebuah showroom mobil bekas di Jalan Arifin Ahmad Blok A-10, belakang Klinik Maryam, menawarkan solusi terbaik bagi kalangan menengah ke bawah. Dalam era perkembangan teknologi dan tren, konsumen cenderung konsumtif dan mudah bosan dengan produk lama. Orang kalangan atas selalu mengikuti perkembangan, memesan mobil terbaru, dan menjual mobil lama.
                        Showroom kami memberikan kemudahan bagi kalangan menengah ke bawah untuk memiliki mobil dengan harga terjangkau dan kondisi yang baik. Meski masih menggunakan pencatatan manual, kami berkomitmen memberikan pelayanan yang efisien dan aman. Lokasi strategis di belakang Klinik Maryam memberikan nilai tambah, meski pendekatan pemasaran kami sebelumnya kurang efektif.
                        Sejalan dengan era globalisasi, Gun Mobilindo menekankan pentingnya peningkatan kualitas sistem informasi untuk mendukung bisnis kami. Pendirian perusahaan ini bertujuan memberikan pengalaman pelanggan yang lebih baik melalui efisiensi, kemudahan, dan keamanan.
                        </p>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
    <!-- contact section end -->
    <!-- footer section start -->
    <div class="footer_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footeer_logo"><img src="{{ asset('images/carpedia_logo2.png') }}"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer section end -->
    <!-- copyright section start -->
    <div class="copyright_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="copyright_text">Carpedia Team</p>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright section end -->
    <!-- Javascript files-->
    <script src="{{ asset('plugins/js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/js/popper.min.js') }}"></script>
    <script src="{{ asset('plugins/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ asset('plugins/js/plugin.js') }}"></script>
    <!-- sidebar -->
    <script src="{{ asset('plugins/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('plugins/js/custom.js') }}"></script>
</body>

</html>
