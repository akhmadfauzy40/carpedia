<!DOCTYPE html>
<html>

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Home - Carpedia</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/bootstrap.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/css/style.css') }}">
    <!-- Responsive-->
    <link rel="stylesheet" href="{{ asset('plugins/css/responsive.css') }}">
    <!-- fevicon -->
    <link rel="icon" href="{{ asset('images/carpedia_logo2_justlogo.png') }}" type="image/gif" />
    <!-- font css -->
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&family=Raleway:wght@400;500;600;700;800&display=swap"
        rel="stylesheet">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/css/jquery.mCustomScrollbar.min.css') }}">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
</head>

<body>
    <!-- header section start -->
    <div class="header_section">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand"href="/"><img src="{{ asset('images/carpedia_logo2.png') }}"
                        style="max-height: 50px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#showroom">Showroom</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <!-- header section end -->
    <div class="call_text_main">
        <div class="container">
            <div class="call_taital">
                <div class="call_text"><a href="https://maps.app.goo.gl/fguMp2LQB4ikJ8RM9" target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i><span
                            class="padding_left_15">Location</span></a></div>
                <div class="call_text"><a href="https://wa.me/+62812682496" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i><span
                            class="padding_left_15">(+62) 812-6823-496</span></a></div>
                <div class="call_text"><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i><span
                            class="padding_left_15">-</span></a></div>
            </div>
        </div>
    </div>
    <!-- banner section start -->
    <div class="banner_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div id="banner_slider" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="banner_taital_main">
                                    <h1 class="banner_taital">Car<span style="color: #fe5b29;">Pedia</span>
                                    </h1>
                                    <p class="banner_text" style="font-size: 20px">Carpedia - Showroom Mobil Second
                                        Amanah dan Terpercaya</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="banner_img"><img src="{{ asset('plugins/images/banner-img.png') }}"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner section end -->
    <!-- about section start -->
    <div class="about_section layout_padding">
        <div class="container">
            <div class="about_section_2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image_iman"><img src="{{ asset('plugins/images/about-img.png') }}"
                                class="about_img"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_taital_box">
                            <h1 class="about_taital">Tentang <span style="color: #fe5b29;">Kami</span></h1>
                            <p class="about_text">Gun Mobilindo adalah showroom mobil bekas yang terletak di Jalan Arifin Ahmad Blok A-10, belakang Klinik Maryam. Kami menyediakan pilihan mobil bekas berkualitas dengan harga terjangkau. Dalam era perkembangan tren dan teknologi, kami memahami kebutuhan dinamis konsumen terhadap mobil yang sporty, stylish, dan ekonomis.
                                Kami hadir sebagai solusi bagi kalangan menengah ke bawah, memberikan alternatif mobil bekas yang terawat dengan baik, tanpa menguras kantong. Berbeda dari showroom lain, Gun Mobilindo tetap mempertahankan pencatatan manual yang memberikan sentuhan personal dalam pelayanan...</p>
                            <div class="readmore_btn"><a href="/about">Read More</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about section end -->
    <div class="search_section" id="showroom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="search_taital">Cari Mobil Impian Anda</h1>
                    <p class="search_text">Silahkan Filter Mobil Untuk Memudahkan Anda Mencari Mobil Yang Anda Inginkan
                    </p>
                    <!-- select box section start -->
                    <form action="/home/filter" method="post" id="filter">
                        @csrf
                        <div class="container">
                            <div class="select_box_section">
                                <div class="select_box_main">
                                    <div class="row">
                                        <div class="col-md-3 select-outline">
                                            <select
                                                class="mdb-select md-form md-outline colorful-select dropdown-primary"
                                                name="merk">
                                                <option value="all">Semua Merk</option>
                                                @foreach ($merks as $merk)
                                                    <option value="{{ $merk }}">{{ $merk }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 select-outline">
                                            <select
                                                class="mdb-select md-form md-outline colorful-select dropdown-primary"
                                                name="jenis">
                                                <option value="all">Semua Jenis</option>
                                                @foreach ($jeniss as $jenis)
                                                    <option value="{{ $jenis }}">{{ $jenis }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3 select-outline">
                                            <select
                                                class="mdb-select md-form md-outline colorful-select dropdown-primary"
                                                name="transmisi">
                                                <option value="all">Semua Transmisi</option>
                                                @foreach ($transmisis as $transmisi)
                                                    <option value="{{ $transmisi }}">{{ $transmisi }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="search_btn"><a href="javascript:void(0);"
                                                    onclick="document.getElementById('filter').submit();">CARI</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- select box section end -->
                </div>
            </div>
        </div>
    </div>
    <!-- gallery section start -->
    <div class="gallery_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="gallery_taital">SHOWROOM</h1>
                </div>
            </div>
            <div class="gallery_section_2">
                <div class="row">
                    @foreach ($datas as $data)
                        <div class="col-md-4 mt-5">
                            <div class="gallery_box">
                                <div class="gallery_img"><img src="{{ asset('storage/' . $data->image) }}"></div>
                                <h3 class="types_text">
                                    {{ $data->merk }}&nbsp;{{ $data->model }}&nbsp;{{ $data->tahun }}</h3>
                                <p class="looking_text">Rp. {{ number_format($data->harga, 0, ',', '.') }}</p>
                                @if ($data->available == 'ya')
                                    <center><button class="btn btn-success mt-2">Tersedia</button></center>
                                    {{-- <div class="read_bt"><a href="https://wa.me/{{ Illuminate\Support\Str::startsWith($data->contact_sales, '0') ? '+62' . substr($data->contact_sales, 1) : $data->contact_sales }}">Hubungi Penjual</a></div> --}}
                                    <div class="read_bt"><a href="/detail-mobil/{{ $data->id }}">Cek Detail</a></div>
                                @else
                                    <center><button class="btn btn-danger mt-2">Tidak Tersedia</button></center>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- contact section end -->
    <!-- footer section start -->
    <div class="footer_section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footeer_logo"><img src="{{ asset('images/carpedia_logo2.png') }}"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer section end -->
    <!-- copyright section start -->
    <div class="copyright_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p class="copyright_text">Carpedia Team</p>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright section end -->
    <!-- Javascript files-->
    <script src="{{ asset('plugins/js/jquery.min.js') }}"></script>
    <script src="{{ asset('plugins/js/popper.min.js') }}"></script>
    <script src="{{ asset('plugins/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('plugins/js/jquery-3.0.0.min.js') }}"></script>
    <script src="{{ asset('plugins/js/plugin.js') }}"></script>
    <!-- sidebar -->
    <script src="{{ asset('plugins/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('plugins/js/custom.js') }}"></script>
</body>

</html>
