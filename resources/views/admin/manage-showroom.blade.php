@extends('admin.layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/carpedia_admin_justLogo.png') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>
    <script>
        new DataTable('#example');
    </script>
    <script src="{{ asset('js/file-upload.js') }}"></script>
@endpush

@section('title')
    Manage Showroom
@endsection

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="index.html"><img src="{{ asset('images/carpedia_admin-logodantext.png') }}"
                        class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('images/carpedia_admin_justLogo.png') }}"
                        alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            <a href="/logout" class="dropdown-item">
                                <i class="ti-power-off text-primary"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-akun">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Manage Akun Sales</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-showroom">
                            <i class="ti-car menu-icon"></i>
                            <span class="menu-title">Manage Showroom</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-order">
                            <i class="ti-shopping-cart menu-icon"></i>
                            <span class="menu-title">Manage Order</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/history-penjualan">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">History Penjualan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-md-12 text-right">
                                            <button class="btn btn-success" data-toggle="modal"
                                                data-target="#tambahMobil">Tambah Mobil</button>
                                        </div>
                                    </div>
                                    <p class="card-title">Data Mobil</p>
                                    <form action="/manage-showroom/filter" method="post">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Merk:</label>
                                                <select class="form-control" name="merk" id="merkFilter">
                                                    <option value="all">Semua Merk</option>
                                                    @foreach ($merks as $merk)
                                                        <option value="{{ $merk }}">{{ $merk }}</option>
                                                    @endforeach
                                                    <!-- Tambahkan opsi merk sesuai kebutuhan -->
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Tahun:</label>
                                                <select class="form-control" name="tahun" id="tahunFilter">
                                                    <option value="all">Semua Tahun</option>
                                                    @foreach ($tahuns as $tahun)
                                                        <option value="{{ $tahun }}">{{ $tahun }}</option>
                                                    @endforeach
                                                    <!-- Tambahkan opsi tahun sesuai kebutuhan -->
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Jenis:</label>
                                                <select class="form-control" name="jenis" id="jenisFilter">
                                                    <option value="all">Semua Jenis</option>
                                                    @foreach ($jeniss as $jenis)
                                                        <option value="{{ $jenis }}">{{ $jenis }}</option>
                                                    @endforeach
                                                    <!-- Tambahkan opsi jenis sesuai kebutuhan -->
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Transmisi:</label>
                                                <select class="form-control" name="transmisi" id="transmisiFilter">
                                                    <option value="all">Semua Transmisi</option>
                                                    <option value="manual">Manual</option>
                                                    <option value="otomatis">Otomatis</option>
                                                    <!-- Tambahkan opsi transmisi sesuai kebutuhan -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">Apply Filter</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>



                        {{-- LIST MOBIL --}}
                        @if ($data->isEmpty())
                            <div class="col-md-12">
                                <p class="text-center">Tidak ada data mobil</p>
                            </div>
                        @else
                            @foreach ($data as $car)
                                <div class="col-md-4 grid-margin stretch-card">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="card-title">{{ $car->merk }} {{ $car->model }}</h4>
                                            <img src="{{ asset('storage/' . $car->image) }}" alt="Mobil Image"
                                                class="img-fluid rounded"
                                                style="width: 100%; height: auto; margin-bottom: 15px;">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="card-text"><strong>Merk:</strong> {{ $car->merk }}</p>
                                                    <p class="card-text"><strong>Model:</strong> {{ $car->model }}</p>
                                                    <p class="card-text"><strong>Tahun:</strong> {{ $car->tahun }}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="card-text"><strong>Jenis:</strong> {{ $car->jenis }}</p>
                                                    <p class="card-text"><strong>Warna:</strong> {{ $car->warna }}</p>
                                                    <p class="card-text"><strong>Harga:</strong> Rp.
                                                        {{ number_format($car->harga, 0, ',', '.') }}</p>
                                                </div>
                                            </div>
                                            <div class="mt-3">
                                                <a href="/cars-detail/{{ $car->id }}" class="btn btn-primary">Detail</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif


                    </div>
                </div>
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="tambahMobil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Mobil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('cars.store') }}" method="POST" id="formTambahMobil"
                        enctype="multipart/form-data">
                        @csrf <!-- Menambahkan token CSRF -->
                        <div class="form-group">
                            <label for="merk">Merk:</label>
                            <input type="text" class="form-control" id="merk" name="merk" required>
                        </div>
                        <div class="form-group">
                            <label for="model">Model:</label>
                            <input type="text" class="form-control" id="model" name="model" required>
                        </div>
                        <div class="form-group">
                            <label for="tahun">Tahun:</label>
                            <input type="text" class="form-control" id="tahun" name="tahun" required>
                        </div>
                        <div class="form-group">
                            <label for="jenis">Jenis:</label>
                            <select class="form-control" id="jenis" name="jenis" required>
                                <option selected disabled>- Silahkan Pilih Jenis Mobil -</option>
                                <option value="sedan">Sedan</option>
                                <option value="hatchback">Hatchback</option>
                                <option value="suv">SUV (Sport Utility Vehicle)</option>
                                <option value="mpv">MPV (Multi-Purpose Vehicle)</option>
                                <option value="coupe">Coupe</option>
                                <option value="convertible">Convertible</option>
                                <option value="truck">Truck</option>
                                <option value="van">Van</option>
                                <option value="crossover">Crossover</option>
                                <option value="electric">Electric</option>
                                <option value="hybrid">Hybrid</option>
                                <option value="luxury">Luxury</option>
                                <option value="sport">Sports Car</option>
                                <option value="wagon">Station Wagon</option>
                                <option value="compact">Compact Car</option>
                                <!-- Tambahkan opsi jenis sesuai kebutuhan -->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="transmisi">Transmisi:</label>
                            <select class="form-control" id="transmisi" name="transmisi" required>
                                <option value="manual">Manual</option>
                                <option value="otomatis">Otomatis</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="warna">Warna:</label>
                            <input type="text" class="form-control" id="warna" name="warna" required>
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga:</label>
                            <input type="text" class="form-control" id="harga" name="harga" required>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi:</label>
                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="harga">Kontak Sales:</label>
                            <input type="text" class="form-control" id="contact_sales" name="contact_sales" required>
                        </div>
                        <div class="form-group">
                            <label>File upload</label>
                            <input type="file" name="image" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled
                                    placeholder="Upload Image">
                                <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
