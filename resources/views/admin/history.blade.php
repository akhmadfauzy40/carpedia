@extends('admin.layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/carpedia_admin_justLogo.png') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>
    <!-- DataTables Buttons JS -->
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script>
        $(document).ready(function() {
            var title = 'Data Penjualan (Seluruh Penjualan)';

            // Cek apakah variabel PHP telah diatur
            var tanggalAwal = @json($tanggal_awal ?? null);
            var tanggalAkhir = @json($tanggal_akhir ?? null);

            // Pengecekan variabel $tanggal_awal dan $tanggal_akhir
            if (tanggalAwal !== null && tanggalAkhir !== null) {
                title = 'Data Penjualan (' + tanggalAwal + ' - ' + tanggalAkhir + ')';
            }

            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excel',
                    text: 'Save to Excel',
                    title: title,
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                }]
            });
        });
    </script>

    <script src="{{ asset('js/file-upload.js') }}"></script>
@endpush

@section('title')
    History Penjualan (Seluruh Penjualan)
@endsection

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="index.html"><img src="{{ asset('images/carpedia_admin-logodantext.png') }}"
                        class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('images/carpedia_admin_justLogo.png') }}"
                        alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            <a href="/logout" class="dropdown-item">
                                <i class="ti-power-off text-primary"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-akun">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Manage Akun Sales</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-showroom">
                            <i class="ti-car menu-icon"></i>
                            <span class="menu-title">Manage Showroom</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-order">
                            <i class="ti-shopping-cart menu-icon"></i>
                            <span class="menu-title">Manage Order</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/history-penjualan">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">History Penjualan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <p class="card-title">Data Penjualan Mobil</p>
                                    <form action="/history/filter" method="post">
                                        @csrf
                                        @if (isset($tanggal_awal) && isset($tanggal_akhir))
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="tanggal_awal">Tanggal Awal</label>
                                                        <input type="date" class="form-control" id="tanggal_awal"
                                                            name="tanggal_awal" value="{{ $tanggal_awal }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="tanggal_akhir">Tanggal Akhir</label>
                                                        <input type="date" class="form-control" id="tanggal_akhir"
                                                            name="tanggal_akhir" value="{{ $tanggal_akhir }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="tanggal_awal">Tanggal Awal</label>
                                                        <input type="date" class="form-control" id="tanggal_awal"
                                                            name="tanggal_awal">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="tanggal_akhir">Tanggal Akhir</label>
                                                        <input type="date" class="form-control" id="tanggal_akhir"
                                                            name="tanggal_akhir">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">Apply Filter</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <p class="card-title">Data Penjualan</p>
                                    <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered"
                                            style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Nama Customer</th>
                                                    <th>Nomor Telpon</th>
                                                    <th>Alamat</th>
                                                    <th>Harga Mobil</th>
                                                    <th>Diskon</th>
                                                    <th>Total Pembayaran</th>
                                                    <th>Sales</th>
                                                    <th>Mobil</th>
                                                    <th>Tanggal Pembelian</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($datas as $no => $data)
                                                    <tr>
                                                        <td>{{ $data->customer }}</td>
                                                        <td>{{ $data->nomor_telpon }}</td>
                                                        <td>{{ $data->alamat }}</td>
                                                        <td>Rp. {{ number_format($data->cars->harga, 0, ',', '.') }}</td>
                                                        <td>{{ $data->diskon }} %</td>
                                                        <td>Rp. {{ number_format($data->total, 0, ',', '.') }}</td>
                                                        <td>{{ $data->user->name }}</td>
                                                        <td>{{ $data->cars->merk }} {{ $data->cars->model }}
                                                            ({{ $data->cars->tahun }})
                                                            {{ $data->cars->warna }}</td>
                                                        <td>{{ $data->created_at }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Nama Customer</th>
                                                    <th>Nomor Telpon</th>
                                                    <th>Alamat</th>
                                                    <th>Harga Mobil</th>
                                                    <th>Diskon</th>
                                                    <th>Total Pembayaran</th>
                                                    <th>Sales</th>
                                                    <th>Mobil</th>
                                                    <th>Tanggal Pembelian</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

    <!-- Modal -->
@endsection
