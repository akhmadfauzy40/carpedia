@extends('admin.layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/carpedia_admin_justLogo.png') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>
    <script>
        new DataTable('#example');
    </script>
    <script src="{{ asset('js/file-upload.js') }}"></script>
@endpush

@section('title')
    Detail Mobil
@endsection

@section('content')
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="index.html"><img src="{{ asset('images/carpedia_admin-logodantext.png') }}"
                        class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('images/carpedia_admin_justLogo.png') }}"
                        alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            <a href="/logout" class="dropdown-item">
                                <i class="ti-power-off text-primary"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-akun">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Manage Akun Sales</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/manage-showroom">
                            <i class="ti-car menu-icon"></i>
                            <span class="menu-title">Manage Showroom</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-order">
                            <i class="ti-shopping-cart menu-icon"></i>
                            <span class="menu-title">Manage Order</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/history-penjualan">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">History Penjualan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $car->merk }} {{ $car->model }}</h4>
                                    <center><img src="{{ asset('storage/' . $car->image) }}" alt="Mobil Image"
                                        class="img-fluid rounded" style="width: 50%; height: auto; margin-bottom: 15px;"></center>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="card-text"><center><strong>Deskripsi Mobil</strong></center></p>
                                            <p class="card-text"><strong>{{ $car->deskripsi }}</strong></p>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <p class="card-text"><strong>Merk:</strong> {{ $car->merk }}</p>
                                            <p class="card-text"><strong>Model:</strong> {{ $car->model }}</p>
                                            <p class="card-text"><strong>Tahun:</strong> {{ $car->tahun }}</p>
                                            <p class="card-text"><strong>Transmisi:</strong> {{ $car->transmisi }}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text"><strong>Jenis:</strong> {{ $car->jenis }}</p>
                                            <p class="card-text"><strong>Warna:</strong> {{ $car->warna }}</p>
                                            <p class="card-text"><strong>Harga:</strong> Rp.
                                                {{ number_format($car->harga, 0, ',', '.') }}</p>
                                            <p class="card-text"><strong>Ketersediaan:</strong>
                                                {{ $car->available == 'ya' ? 'Tersedia' : 'Tidak Tersedia / Terjual' }}</p>
                                        </div>
                                    </div>

                                    <div class="mt-3">
                                        <!-- Tambahkan link atau tombol untuk kembali ke halaman sebelumnya -->
                                        <button class="btn btn-success" data-toggle="modal"
                                                data-target="#update">Update Data Mobil</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Data Mobil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/cars-update/{{ $car->id }}" method="POST" id="formTambahMobil"
                        enctype="multipart/form-data">
                        @csrf <!-- Menambahkan token CSRF -->
                        @method('PUT')
                        <div class="form-group">
                            <label for="merk">Merk:</label>
                            <input type="text" class="form-control" id="merk" value="{{ $car->merk }}" name="merk" required>
                        </div>
                        <div class="form-group">
                            <label for="model">Model:</label>
                            <input type="text" class="form-control" id="model" value="{{ $car->model }}" name="model" required>
                        </div>
                        <div class="form-group">
                            <label for="tahun">Tahun:</label>
                            <input type="text" class="form-control" id="tahun" value="{{ $car->tahun }}" name="tahun" required>
                        </div>
                        <div class="form-group">
                            <label for="jenis">Jenis:</label>
                            <select class="form-control" id="jenis" name="jenis" required>
                                <option value="sedan" {{ $car->jenis === 'sedan' ? 'selected' : '' }}>Sedan</option>
                                <option value="hatchback" {{ $car->jenis === 'hatchback' ? 'selected' : '' }}>Hatchback</option>
                                <option value="suv" {{ $car->jenis === 'suv' ? 'selected' : '' }}>SUV (Sport Utility Vehicle)</option>
                                <option value="mpv" {{ $car->jenis === 'mpv' ? 'selected' : '' }}>MPV (Multi-Purpose Vehicle)</option>
                                <option value="coupe" {{ $car->jenis === 'coupe' ? 'selected' : '' }}>Coupe</option>
                                <option value="convertible" {{ $car->jenis === 'convertible' ? 'selected' : '' }}>Convertible</option>
                                <option value="truck" {{ $car->jenis === 'truck' ? 'selected' : '' }}>Truck</option>
                                <option value="van" {{ $car->jenis === 'van' ? 'selected' : '' }}>Van</option>
                                <option value="crossover" {{ $car->jenis === 'crossover' ? 'selected' : '' }}>Crossover</option>
                                <option value="electric" {{ $car->jenis === 'electric' ? 'selected' : '' }}>Electric</option>
                                <option value="hybrid" {{ $car->jenis === 'hybrid' ? 'selected' : '' }}>Hybrid</option>
                                <option value="luxury" {{ $car->jenis === 'luxury' ? 'selected' : '' }}>Luxury</option>
                                <option value="sport" {{ $car->jenis === 'sport' ? 'selected' : '' }}>Sports Car</option>
                                <option value="wagon" {{ $car->jenis === 'wagon' ? 'selected' : '' }}>Station Wagon</option>
                                <option value="compact" {{ $car->jenis === 'compact' ? 'selected' : '' }}>Compact Car</option>
                                <!-- Tambahkan opsi jenis sesuai kebutuhan -->
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="transmisi">Transmisi:</label>
                            <select class="form-control" id="transmisi" name="transmisi" required>
                                <option value="manual" {{ $car->transmisi === 'manual' ? 'selected' : '' }}>Manual</option>
                                <option value="otomatis" {{ $car->transmisi === 'otomatis' ? 'selected' : '' }}>Otomatis</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="warna">Warna:</label>
                            <input type="text" class="form-control" id="warna" value="{{ $car->warna }}" name="warna" required>
                        </div>
                        <div class="form-group">
                            <label for="harga">Harga:</label>
                            <input type="text" class="form-control" id="harga" value="{{ $car->harga }}" name="harga" required>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi:</label>
                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" required>{{ $car->deskripsi }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="harga">Kontak Sales:</label>
                            <input type="text" class="form-control" id="contact_sales" value="{{ $car->contact_sales }}" name="contact_sales" required>
                        </div>
                        <div class="form-group">
                            <label>Upload Gambar (Kosongkan jika tidak ingin mengganti gambar)</label>
                            <input type="file" name="image" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled
                                    placeholder="Upload Image">
                                <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
