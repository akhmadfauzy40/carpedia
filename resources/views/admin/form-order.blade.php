@extends('admin.layout.master')

@push('styles')
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/carpedia_admin_justLogo.png') }}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.css">
@endpush

@push('scripts')
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap4.min.js"></script>
    <script>
        new DataTable('#example');
    </script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            // Data dummy untuk autofill
            var customerNames = @json($customers->pluck('customer')->unique()->toArray());

            // Inisialisasi Typeahead pada input Nama Customer
            $('#exampleInputUsername1.typeahead').typeahead({
                source: customerNames,
                autoSelect: true,
                items:1
            });
        });
    </script>
@endpush

@section('title')
    Form Order
@endsection

@section('content')

    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                <a class="navbar-brand brand-logo mr-5" href="index.html"><img src="{{ asset('images/carpedia_admin-logodantext.png') }}"
                        class="mr-2" alt="logo" /></a>
                <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('images/carpedia_admin_justLogo.png') }}"
                        alt="logo" /></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/icon-user.png') }}" alt="profile" />
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            <a href="/logout" class="dropdown-item">
                                <i class="ti-power-off text-primary"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">
                            <i class="icon-grid menu-icon"></i>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-akun">
                            <i class="icon-layout menu-icon"></i>
                            <span class="menu-title">Manage Akun Sales</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/manage-showroom">
                            <i class="ti-car menu-icon"></i>
                            <span class="menu-title">Manage Showroom</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/manage-order">
                            <i class="ti-shopping-cart menu-icon"></i>
                            <span class="menu-title">Manage Order</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/history-penjualan">
                            <i class="ti-time menu-icon"></i>
                            <span class="menu-title">History Penjualan</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $car->merk }} {{ $car->model }}</h4>
                                    <center><img src="{{ asset('storage/' . $car->image) }}" alt="Mobil Image"
                                            class="img-fluid rounded"
                                            style="width: 50%; height: auto; margin-bottom: 15px;"></center>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="card-text">
                                                <center><strong>Deskripsi Mobil</strong></center>
                                            </p>
                                            <p class="card-text"><strong>{{ $car->deskripsi }}</strong></p>
                                        </div>
                                    </div>

                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <p class="card-text"><strong>Merk:</strong> {{ $car->merk }}</p>
                                            <p class="card-text"><strong>Model:</strong> {{ $car->model }}</p>
                                            <p class="card-text"><strong>Tahun:</strong> {{ $car->tahun }}</p>
                                            <p class="card-text"><strong>Transmisi:</strong> {{ $car->transmisi }}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="card-text"><strong>Jenis:</strong> {{ $car->jenis }}</p>
                                            <p class="card-text"><strong>Warna:</strong> {{ $car->warna }}</p>
                                            <p class="card-text"><strong>Harga:</strong> Rp.
                                                {{ number_format($car->harga, 0, ',', '.') }}</p>
                                            <p class="card-text"><strong>Ketersediaan:</strong>
                                                {{ $car->available ? 'Tersedia' : 'Tidak Tersedia' }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">FORM PEMESANAN</h4>
                                    <div class="row mt-2">
                                        <div class="col-md-6">
                                            <form class="forms-sample" method="POST"
                                                action="/submit-order/{{ $car->id }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="exampleInputUsername1">Nama Customer</label>
                                                    <input type="text" class="form-control typeahead"
                                                        id="exampleInputUsername1" placeholder="Nama Customer"
                                                        name="customer">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Nomor Telpon</label>
                                                    <input type="text" class="form-control" id="exampleInputEmail1"
                                                        placeholder="Nomor Telpon" name="nomor_telpon">
                                                </div>
                                                <div class="form-group">
                                                    <label for="diskon">Diskon</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">%</span>
                                                        </div>
                                                        <input type="number" class="form-control" placeholder="Diskon"
                                                            aria-label="Diskon" name="diskon">
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                                <button class="btn btn-light">Cancel</button>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="alamat">Alamat</label>
                                                <textarea class="form-control" id="alamat" name="alamat" rows="4"></textarea>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

    <!-- Modal -->
@endsection
