<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\manageOrderController;
use App\Http\Controllers\ManageSalesController;
use App\Http\Controllers\ManageShowroomController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::post('/home/filter', [HomeController::class, 'filter']);
Route::get('/about', function(){
    return view('about-us');
});
Route::get('/detail-mobil/{id}', [HomeController::class, 'detail']);


// ROUTE KHUSUS ADMIN

Route::get('/login', function(){
    return view('admin.login');
})->name('login');

Route::get('/register', function(){
    return view('admin.register');
});

Route::post('/register/store', [AuthController::class, 'store']);

Route::post('/login', [AuthController::class, 'login']);
Route::get('logout', [AuthController::class, 'logout']);


Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index']);
    Route::get('/manage-akun', [ManageSalesController::class, 'index'])->name('manage-akun');
    Route::post('/change-status/{id}', [ManageSalesController::class, 'change_role']);
    Route::post('/delete-sales/{id}', [ManageSalesController::class, 'destroy']);
    Route::get('/manage-showroom', [ManageShowroomController::class, 'index'])->name('manage-showroom');
    Route::post('/cars/store', [ManageShowroomController::class, 'store'])->name('cars.store');
    Route::post('/manage-showroom/filter', [ManageShowroomController::class, 'filter']);
    Route::get('/cars-detail/{id}', [ManageShowroomController::class, 'detail']);
    Route::put('/cars-update/{id}', [ManageShowroomController::class, 'update']);
    Route::get('/manage-order', [manageOrderController::class, 'index'])->name('manage.order');
    Route::post('/manage-order/filter', [manageOrderController::class, 'filter']);
    Route::get('/order-form/{id}', [manageOrderController::class, 'order_form']);
    Route::post('/submit-order/{id}', [manageOrderController::class, 'submit_order']);
    Route::get('/history-penjualan', [HistoryController::class, 'index']);
    Route::post('/history/filter', [HistoryController::class, 'filter']);
});
