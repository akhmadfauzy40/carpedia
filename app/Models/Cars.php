<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    use HasFactory;

    protected $table = 'cars';
    protected $fillable = ['merk', 'model', 'tahun', 'jenis', 'transmisi', 'warna', 'harga', 'deskripsi', 'available', 'contact_sales'];

    public function order()
    {
        return $this->hasOne(Order::class, 'cars_id', 'id');
    }
}
