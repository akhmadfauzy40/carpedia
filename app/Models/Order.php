<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'order';

    protected $fillable = ['customer', 'nomor_telpon', 'alamat', 'diskon', 'total', 'sales_id', 'cars_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'sales_id', 'id');
    }

    public function cars()
    {
        return $this->belongsTo(Cars::class, 'cars_id', 'id');
    }
}
