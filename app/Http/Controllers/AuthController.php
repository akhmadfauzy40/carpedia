<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        // Cek apakah table user masih kosong
        $isTableEmpty = User::first() === null;

        if ($isTableEmpty) {
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->role = "aktif";
            $user->save();
        }else{
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->role = "non-aktif";
            $user->save();
        }

        return view('admin.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        // Coba melakukan otentikasi
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->role === 'aktif') {
                // Otentikasi berhasil
                return redirect('/dashboard')->with('success', 'Login berhasil');
            } else {
                Auth::logout();
                return redirect()->route('login')->with('error', 'Akun Anda nonaktif');
            }
        }

        // Otentikasi gagal
        return redirect()->route('login')->with('error', 'Email atau password salah');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
