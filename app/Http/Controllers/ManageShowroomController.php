<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use Illuminate\Support\Facades\Storage;

class ManageShowroomController extends Controller
{

    public function index()
    {
        $data = Cars::latest()->get();

        // Mengambil nilai unik dari kolom 'merk'
        $merks = Cars::distinct('merk')->pluck('merk');

        // Mengambil nilai unik dari kolom 'tahun'
        $tahuns = Cars::distinct('tahun')->pluck('tahun');

        // Mengambil nilai unik dari kolom 'jenis'
        $jeniss = Cars::distinct('jenis')->pluck('jenis');

        // Mengambil nilai unik dari kolom 'transmisi'
        $transmisis = Cars::distinct('transmisi')->pluck('transmisi');

        return view('admin.manage-showroom', ['data' => $data, 'merks' => $merks, 'tahuns' => $tahuns, 'jeniss' => $jeniss, 'transmisis' => $transmisis]);
    }

    public function store(Request $request)
    {
        // Validasi data dari formulir
        $request->validate([
            'merk' => 'required|string',
            'model' => 'required|string',
            'tahun' => 'required|numeric',
            'jenis' => 'required|string',
            'transmisi' => 'required|string|in:manual,otomatis',
            'warna' => 'required|string',
            'harga' => 'required|numeric',
            'deskripsi' => 'required|string',
            'contact_sales' => 'required|string',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        // Simpan data mobil
        $cars = new Cars([
            'merk' => $request->get('merk'),
            'model' => $request->get('model'),
            'tahun' => $request->get('tahun'),
            'jenis' => $request->get('jenis'),
            'transmisi' => $request->get('transmisi'),
            'warna' => $request->get('warna'),
            'harga' => $request->get('harga'),
            'deskripsi' => $request->get('deskripsi'),
            'contact_sales' => $request->get('contact_sales'),
        ]);

        // Simpan gambar
        // Mendapatkan ekstensi file yang diunggah
        $extension = $request->file('image')->extension();

        // Membangun nama file dengan format merk_jenis_angka_random
        $fileName = $request->get('merk') . '_' . $request->get('jenis') . '_' . uniqid() . '.' . $extension;

        // Menyimpan file dengan nama yang sudah dibangun
        $imagePath = $request->file('image')->storeAs('uploads', $fileName, 'public');
        $cars->image = $imagePath;


        // Simpan data mobil dan gambar
        $cars->save();

        return redirect()->route('manage-showroom')->with('success', 'Data mobil berhasil disimpan.');
    }

    public function filter(Request $request)
    {
        $merkFilter = $request->input('merk');
        $tahunFilter = $request->input('tahun');
        $jenisFilter = $request->input('jenis');
        $transmisiFilter = $request->input('transmisi');

        $query = Cars::query();

        if ($merkFilter && $merkFilter != 'all') {
            $query->where('merk', $merkFilter);
        }

        if ($tahunFilter && $tahunFilter != 'all') {
            $query->where('tahun', $tahunFilter);
        }

        if ($jenisFilter && $jenisFilter != 'all') {
            $query->where('jenis', $jenisFilter);
        }

        if ($transmisiFilter && $transmisiFilter != 'all') {
            $query->where('transmisi', $transmisiFilter);
        }

        $filteredData = $query->get();

        return view('admin.manage-showroom', [
            'data' => $filteredData,
            'merks' => Cars::distinct('merk')->pluck('merk'),
            'tahuns' => Cars::distinct('tahun')->pluck('tahun'),
            'jeniss' => Cars::distinct('jenis')->pluck('jenis'),
            'transmisis' => Cars::distinct('transmisi')->pluck('transmisi'),
        ]);
    }

    public function detail($id)
    {
        $data = Cars::find($id);

        return view('admin.cars-detail', ['car' => $data]);
    }

    public function update(Request $request, $id)
    {
        // Validasi input form
        $request->validate([
            'merk' => 'required',
            'model' => 'required',
            'tahun' => 'required|numeric',
            'jenis' => 'required',
            'transmisi' => 'required|in:manual,otomatis',
            'warna' => 'required',
            'harga' => 'required|numeric',
            'deskripsi' => 'required',
            'contact_sales' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', // Sesuaikan validasi untuk gambar
        ]);

        // Ambil data mobil berdasarkan ID
        $car = Cars::findOrFail($id);

        // Update data mobil
        $car->merk = $request->merk;
        $car->model = $request->model;
        $car->tahun = $request->tahun;
        $car->jenis = $request->jenis;
        $car->transmisi = $request->transmisi;
        $car->warna = $request->warna;
        $car->harga = $request->harga;
        $car->deskripsi = $request->deskripsi;
        $car->contact_sales = $request->contact_sales;

        // Update gambar jika ada file yang diunggah
        if ($request->hasFile('image')) {
            if (Storage::disk('public')->exists($car->image)) {
                Storage::disk('public')->delete($car->image);
            }
            // Simpan gambar
            // Mendapatkan ekstensi file yang diunggah
            $extension = $request->file('image')->extension();

            // Membangun nama file dengan format merk_jenis_angka_random
            $fileName = $request->get('merk') . '_' . $request->get('jenis') . '_' . uniqid() . '.' . $extension;

            // Menyimpan file dengan nama yang sudah dibangun
            $imagePath = $request->file('image')->storeAs('uploads', $fileName, 'public');
            $car->image = $imagePath;
        }

        // Simpan perubahan
        $car->save();

        return redirect()->back()->with('success', 'Data mobil berhasil diperbarui.');
    }
}
