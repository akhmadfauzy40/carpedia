<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function index()
    {
        $data = Order::all();

        return view('admin.history', ['datas' => $data]);
    }

    public function filter(Request $request)
    {
        // Validasi input form
        $request->validate([
            'tanggal_awal' => 'required|date',
            'tanggal_akhir' => 'required|date|after_or_equal:tanggal_awal',
        ]);

        // Ambil data dari form
        $tanggalAwal = $request->input('tanggal_awal');
        $tanggalAkhir = $request->input('tanggal_akhir');

        // Filter data berdasarkan tanggal
        $data = Order::whereBetween('created_at', [$tanggalAwal . ' 00:00:00', $tanggalAkhir . ' 23:59:59'])->get();


        // ... Lakukan sesuatu dengan data yang difilter

        // Kembalikan response atau tampilkan view yang sesuai
        return view('admin.history', ['datas' => $data, 'tanggal_awal' => $tanggalAwal, 'tanggal_akhir' => $tanggalAkhir]);
    }
}
