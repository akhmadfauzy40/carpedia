<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Cars;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        Carbon::setLocale('id');
        $tahun = [];
        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;
        $currentMonth_name = Carbon::now()->format('F', 'id');
        $sales = [];

        for ($i = 5; $i >= 0; $i--) {
            $tahun[] = $currentYear - $i;
            $thnNow = $currentYear - $i;

            $totalSale = Order::whereYear('created_at', $thnNow)->count();
            $sales[] = $totalSale;
        }

        $available_cars = Cars::where('available', 'ya')->count();
        $unavailable_cars = Cars::where('available', 'tidak')->count();
        $cars_sold = Order::whereMonth('created_at', $currentMonth)->count();
        $total_customer = Order::select(DB::raw('COUNT(DISTINCT customer) as total'))->get()[0]->total;


        return view('admin.dashboard', ['tahuns' => $tahun, 'sales'=>$sales, 'available_cars' => $available_cars, 'unavailable_cars' => $unavailable_cars, 'cars_sold' => $cars_sold, 'total_customer' => $total_customer, 'current_year' => $currentYear, 'current_month' => $currentMonth_name]);
    }
}
