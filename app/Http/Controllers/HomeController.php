<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;

class HomeController extends Controller
{
    public function index()
    {
        $data = Cars::where('available', 'ya')->latest()->get();

        // Mengambil nilai unik dari kolom 'merk'
        $merk = Cars::distinct('merk')->pluck('merk');

        // Mengambil nilai unik dari kolom 'jenis'
        $jenis = Cars::distinct('jenis')->pluck('jenis');

        // Mengambil nilai unik dari kolom 'transmisi'
        $transmisi = Cars::distinct('transmisi')->pluck('transmisi');

        return view('welcome', ['datas' => $data, 'merks' => $merk, 'jeniss' => $jenis, 'transmisis' => $transmisi]);
    }

    public function filter(Request $request)
    {
        $merkFilter = $request->input('merk');
        $jenisFilter = $request->input('jenis');
        $transmisiFilter = $request->input('transmisi');

        $query = Cars::query();

        if ($merkFilter && $merkFilter != 'all') {
            $query->where('merk', $merkFilter);
        }

        if ($jenisFilter && $jenisFilter != 'all') {
            $query->where('jenis', $jenisFilter);
        }

        if ($transmisiFilter && $transmisiFilter != 'all') {
            $query->where('transmisi', $transmisiFilter);
        }

        $filteredData = $query->where('available', 'ya')->latest()->get();

        return view('welcome', [
            'datas' => $filteredData,
            'merks' => Cars::distinct('merk')->pluck('merk'),
            'jeniss' => Cars::distinct('jenis')->pluck('jenis'),
            'transmisis' => Cars::distinct('transmisi')->pluck('transmisi'),
        ]);
    }

    public function detail($id)
    {
        $data = Cars::find($id);

        return view('detail-cars', ['data'=>$data]);
    }
}
