<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ManageSalesController extends Controller
{
    public function index()
    {
        // Mengambil semua data sales selain dirinya sendiri
        $currentUserId = auth()->user()->id;

        $data_sales = User::where('id', '!=', $currentUserId)->get();

        return view('admin.manage-user', ['data' => $data_sales]);
    }

    public function change_role(Request $request, $id)
    {
        $data = User::find($id);

        $data->role = $request->role;
        $data->save();

        return redirect('manage-akun');
    }

    public function destroy($id)
    {
        $data = User::find($id);

        $data->delete();

        return redirect('manage-akun');
    }
}
