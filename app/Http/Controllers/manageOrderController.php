<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;

class manageOrderController extends Controller
{
    public function index()
    {
        $data = Cars::latest()->get();

        // Mengambil nilai unik dari kolom 'merk'
        $merks = Cars::distinct('merk')->pluck('merk');

        // Mengambil nilai unik dari kolom 'tahun'
        $tahuns = Cars::distinct('tahun')->pluck('tahun');

        // Mengambil nilai unik dari kolom 'jenis'
        $jeniss = Cars::distinct('jenis')->pluck('jenis');

        // Mengambil nilai unik dari kolom 'transmisi'
        $transmisis = Cars::distinct('transmisi')->pluck('transmisi');

        return view('admin.manage-order', ['data' => $data, 'merks' => $merks, 'tahuns' => $tahuns, 'jeniss' => $jeniss, 'transmisis' => $transmisis]);
    }

    public function filter(Request $request)
    {
        $merkFilter = $request->input('merk');
        $tahunFilter = $request->input('tahun');
        $jenisFilter = $request->input('jenis');
        $transmisiFilter = $request->input('transmisi');

        $query = Cars::query();

        if ($merkFilter && $merkFilter != 'all') {
            $query->where('merk', $merkFilter);
        }

        if ($tahunFilter && $tahunFilter != 'all') {
            $query->where('tahun', $tahunFilter);
        }

        if ($jenisFilter && $jenisFilter != 'all') {
            $query->where('jenis', $jenisFilter);
        }

        if ($transmisiFilter && $transmisiFilter != 'all') {
            $query->where('transmisi', $transmisiFilter);
        }

        $filteredData = $query->get();

        return view('admin.manage-order', [
            'data' => $filteredData,
            'merks' => Cars::distinct('merk')->pluck('merk'),
            'tahuns' => Cars::distinct('tahun')->pluck('tahun'),
            'jeniss' => Cars::distinct('jenis')->pluck('jenis'),
            'transmisis' => Cars::distinct('transmisi')->pluck('transmisi'),
        ]);
    }

    public function order_form($id)
    {
        $data = Cars::find($id);

        $customers = Order::select('customer')->distinct()->get();

        return view('admin.form-order', ['car' => $data, 'customers'=>$customers]);
    }

    public function submit_order(Request $request, $id)
    {
        $request->validate([
            'customer' => 'required',
            'nomor_telpon' => 'required',
            'alamat' => 'required',
            'diskon' => 'required'
        ]);

        $data = new Order;

        $data->customer = $request->customer;
        $data->nomor_telpon = $request->nomor_telpon;
        $data->alamat = $request->alamat;
        $data->diskon = $request->diskon;

        $data_mobil = Cars::find($id);

        $diskon = $data_mobil->harga * $data->diskon / 100;
        $total = $data_mobil->harga - $diskon;

        $data->total = $total;
        $data->sales_id = Auth::id();
        $data->cars_id = $id;

        $data_mobil->available = 'tidak';

        $data_mobil->save();
        $data->save();

        return redirect('/manage-order');

    }
}
